/*
 ** Single Axis Mill Controller
 ** LICENSE: GNU General Public License, version 3 (GPL-3.0)
 */
 
 /*
  ** TODO:
  ** check length of display text and adjust as appropriate
  ** Implement enable pin (to freewheel stepper) - possibly done
  ** Implement total length text - possibly done
 */
 
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <rotary.h>
#include <OneButton.h>
#include <AccelStepper.h>

// LCD - 4 row.
#define I2C_ADDR    0x27  // Define I2C Address where the PCF8574A lcd is
#define BACKLIGHT_PIN     3
#define En_pin  2
#define Rw_pin  1
#define Rs_pin  0
#define D4_pin  4
#define D5_pin  5
#define D6_pin  6
#define D7_pin  7

/*  Connections:
 **  Rotary encoder connects to D2 and D3
 **  Mode button from rotary encoder connects to GND and A0
 **  Reset button connects to GND and A1
 **  Go button connects to GND and A2
 **  LCD uses I2C bus, which is pins A4 and A5 on the arduino nano
 **  Stepper driver connects to D8 (pul+) and D9 (dir+). Pul- and dir- go to ground
 **  Stepper - a+ red, a- blue, b+ green, b- black
 **  Endstops connect to D10 (both to same) and GND
 **  Writing 5v to the enable+ (whilst enable- is GND) lets the stepper freewheel - pin D11
*/

const int stepsPerMM = 201; //steps per mm on the leadscrew

double distToTravel = 0; // The distance to travel in mm
double distToGo = 0;
double totDistance = 0;  // The total distance travelled
int eSTOP = 0;           // If there is an emergency stop event, this goes to 1

double increment = 1; // the increment to move, can be 1 or 0.05
int incrFLAG = 0;        // Global flag for toggling increment mode




int i = 0;               // updating the lcd (it's slow, so had to be separate)

Rotary r = Rotary(2, 3);
LiquidCrystal_I2C lcd(I2C_ADDR,En_pin,Rw_pin,Rs_pin,D4_pin,D5_pin,D6_pin,D7_pin);
OneButton buttonMode(A0,true);
OneButton buttonReset(A1,true); // Also the Emergency stop button
OneButton buttonGO(A2,true);
AccelStepper stepper(AccelStepper::DRIVER,8,9);

void setup()
{
  Serial.begin(9600);
  lcd.begin (20,4,LCD_5x8DOTS);
 
  lcd.setBacklightPin(BACKLIGHT_PIN,POSITIVE); // init the backlight
  lcd.home ();                   // go home    
  lcd.print(" MOVE       0.0  mm ");
  lcd.setCursor ( 0, 1 );        // go to the next line
  lcd.print(" Remaining  0.0  mm ");
  lcd.setCursor ( 0, 2 );        // go to the next line
  lcd.print(" Mode: 0.05 mm");
  lcd.setCursor ( 0, 3 );        // go to the next line
  lcd.print(" TOT DIST   0.0  mm");
  lcd.setBacklight(HIGH);     // Backlight on
 
  //Rotary encoder setup
  PCICR |= (1 << PCIE2);
  PCMSK2 |= (1 << PCINT18) | (1 << PCINT19);
  sei();
 
  // mode button - toggles between 0.05mm and 1mm
  buttonMode.setPressTicks(350);
  buttonMode.attachLongPressStop(longPressMode);
 
  // Reset button
  buttonReset.attachClick(clickReset);
  buttonReset.attachLongPressStop(longPressReset);
 
  // GO Button
  buttonGO.attachClick(moveStepper);
 
  // Endstop
  pinMode(10,INPUT_PULLUP);
  
  // setup pins
  pinMode(A0,INPUT_PULLUP);
  pinMode(A1,INPUT_PULLUP);
  pinMode(A2,INPUT_PULLUP);
 
  stepper.setMaxSpeed(1200);
  stepper.setAcceleration(400);
  stepper.setEnablePin(11);
}
 
void loop()
{
  // keep watching the push button: 
  buttonMode.tick();
  buttonReset.tick();
  buttonGO.tick();
 
  // Check endstop
  if (digitalRead(10) == LOW)
  {
    eSTOP = 1;
    distToGo = stepper.distanceToGo() / stepsPerMM;
    totDistance = totDistance + distToTravel - distToGo;
    stepper.stop();
    stepper.setCurrentPosition(0);
  }
 
  stepper.run();
 
  i++;
  if (i == 3000)
  {
    updateLCD();
    i = 0;
  } 

  if (stepper.distanceToGo() == 0)
  {
    stepper.disableOutputs();
    if (stepper.targetPosition() != 0)
      totDistance = totDistance + distToTravel;
    stepper.setCurrentPosition(0);
  }

}

void updateLCD()
{
  if (!stepper.isRunning())
  {
    lcd.setCursor(11,0);
    String dist = String(distToTravel,2) + " mm ";

  if (dist.length() > 8)
    dist.trim();
    lcd.print(dist);
    lcd.setCursor ( 0, 3 );        // go to the next line
    String temp = " TOT DIST   " + String(totDistance,2) + " mm ";
  if (temp.length() > 19)
      temp.trim();





    lcd.print(temp);
  }
  lcd.setCursor(11,1);
  if (eSTOP == 0)
    distToGo = stepper.distanceToGo() / stepsPerMM;
  String distTG = String(distToGo,2) + " mm ";
  lcd.print(distTG);
}

// ------------------------------------------------
// Rotary encoder - interrupt driven, doesn't need to be in the loop

ISR(PCINT2_vect)
{
  unsigned char result = r.process();
  if (result)
  {
    int direction;
    if (result == DIR_CW) {
      direction = -1;
    } else {
      direction = 1;
    }
    distToTravel = distToTravel + increment * direction;
  }
}

// For the rotary encoder to change scale
void longPressMode()
{ 
  // Increment setup
  if (incrFLAG == 1)
  {
    increment = 1;
    incrFLAG = 0;
    lcd.setCursor(0, 2);
    lcd.print(" Mode: 1 mm   ");
  }
  else if (incrFLAG == 0)
  {
    increment = 0.05;
    incrFLAG = 1;
    lcd.setCursor(0, 2);
    lcd.print(" Mode: 0.05 mm");
  } 
 } 
 
void clickReset()
{
  eSTOP = 1;
  distToGo = stepper.distanceToGo() / stepsPerMM;
  totDistance = totDistance + distToTravel - distToGo;
  stepper.stop();
  stepper.setCurrentPosition(0);
}
 
void longPressReset()
{
  stepper.stop();
  stepper.setCurrentPosition(0);
  // reset the total distance counter
  totDistance = 0;
  // reset distance to travel
  distToTravel = 0;
  eSTOP = 0;
}
 
void moveStepper()
{
  if (distToTravel != 0)
  {
    eSTOP = 0;
    stepper.enableOutputs();
    stepper.moveTo(-distToTravel * stepsPerMM);
  }
}
